package com.example.poko.foldingcellinshorts;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Poko on 12-07-2016.
 */
public interface APIshortNews {

    @GET("stories")
    Call<StoriesResponse> fetchStories(@Query("page") int pageNo);
}
