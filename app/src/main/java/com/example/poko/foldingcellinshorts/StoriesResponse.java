
package com.example.poko.foldingcellinshorts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StoriesResponse {


    @SerializedName("data")
    @Expose
    private List<SingleStory> story = new ArrayList<SingleStory>();


    public List<SingleStory> getstory() {
        return story;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setstory(List<SingleStory> data) {
        this.story = data;
    }

}
