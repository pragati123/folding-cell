package com.example.poko.foldingcellinshorts;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ramotion.foldingcell.FoldingCell;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Poko on 12-07-2016.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    public FoldingCell foldingCell;
    public FrameLayout cell;
    private TextView mTextView;
    private TextView mContentTextView;
    private TextView mtitletextview;
    private TextView mCreatedAtTextView;
    private TextView mtimetext;
    boolean isOpen = false;
    public SingleStory mSingleStories;
    public CircleImageView image;
    public ImageView contentImage;
    public Context mContext;
    public long millis;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public ViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();

        foldingCell = (FoldingCell) itemView.findViewById(R.id.folding_cell);
        mTextView = (TextView) itemView.findViewById(R.id.text);
        mContentTextView = (TextView) itemView.findViewById(R.id.context_description_textview);
        mtitletextview = (TextView) itemView.findViewById(R.id.context_title_textView);
        mCreatedAtTextView = (TextView) itemView.findViewById(R.id.context_createdat_textview);
        mtimetext = (TextView) itemView.findViewById(R.id.time_text);
        image = (CircleImageView) itemView.findViewById(R.id.imgProfilePicture);
        contentImage = (ImageView) itemView.findViewById(R.id.context_url_imageview);

    }

    public void onbind() {
        Log.d("Codekamp", "done 5");

        this.mTextView.setText(mSingleStories.getTitle());
        Picasso.with(mContext).load(mSingleStories.getThumbnailUrl()).into(image);
        String toParse = mSingleStories.getCreatedAt();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(toParse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
         millis = date.getTime();
        mtimetext.setText(String.valueOf(getTimeAgo(millis, mContext)));


        foldingCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foldingCell.toggle(false);
                isOpen = true;
                mContentTextView.setText(mSingleStories.getDescription());
                Picasso.with(mContext).load(mSingleStories.getThumbnailUrl()).into(contentImage);
                mtitletextview.setText(mSingleStories.getTitle());
                mCreatedAtTextView.setText(String.valueOf(getTimeAgo(millis, mContext)));


            }
        });


        if (this.isOpen) {
            foldingCell.toggle(true);
            isOpen = false;
        }


      /*  else {
            foldingCell.toggle(false);
        }*/


        //  mTextView.setText(Integer.toString(position) + "  " + Integer.toString(position % 2));

    }

    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = new Date().getTime();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

}
