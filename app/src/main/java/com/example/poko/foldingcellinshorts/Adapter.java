package com.example.poko.foldingcellinshorts;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Poko on 12-07-2016.
 */
public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    public List<SingleStory> adapterOfList;

    public Adapter(List<SingleStory> list) {
        this.adapterOfList = list;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_single_cell, null);
        Log.d("Codekamp","done 2");

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       // holder.isOpen=false;
        holder.mSingleStories= this.adapterOfList.get(position);
        Log.d("Codekamp","done 3");

        holder.onbind();
    }

    @Override
    public int getItemCount() {
        Log.d("Codekamp","done 4");

        return adapterOfList.size();
    }
}
