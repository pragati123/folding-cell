package com.example.poko.foldingcellinshorts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.ramotion.foldingcell.FoldingCell;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
   public RecyclerView mRecyclerView;
    public List<SingleStory> mSingleStories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView=(RecyclerView)findViewById(R.id.selectedsongsList_recycleerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://news.vaetas.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        APIshortNews newsShort_api = retrofit.create(APIshortNews.class);
       Call<StoriesResponse> call= newsShort_api.fetchStories(1);
        Log.d("Codekamp","done 1");

        call.enqueue(new Callback<StoriesResponse>() {
            @Override
            public void onResponse(Call<StoriesResponse> call, Response<StoriesResponse> response) {
                Log.d("Codekamp","done 5");

                mSingleStories=response.body().getstory();
                Adapter adapter=new Adapter(mSingleStories);
                mRecyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<StoriesResponse> call, Throwable t) {
                Log.d("Codekamp", t.getMessage());
                t.printStackTrace();
            }
        });





    }
}
